package Baranski_Jakub_5;

import com.google.protobuf.InvalidProtocolBufferException;
import org.jgroups.Address;
import org.jgroups.Message;
import org.jgroups.Receiver;
import org.jgroups.View;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * Created by baran on 13.05.16.
 */
public class ChatManagementReceiver implements Receiver {

    private Map<String, List<String>> chanelUsers;
    private Client client;

    public ChatManagementReceiver(Client client) {
        this.client = client;
        chanelUsers = new HashMap<>(200);
        for (int i = 1; i <= 200; i++) {
            chanelUsers.put(String.valueOf(i), new ArrayList<>());
        }
    }

    @Override
    public void viewAccepted(View view) {
        System.out.println(view + " view");
        List<Address> members = view.getMembers();
        for (List<String> channel : chanelUsers.values()) {
            List<String> toRemove = new ArrayList<>();
            for (String user : channel) {
                Optional<Address> any = members.stream().filter(member -> member.toString().equals(user)).findAny();
                if (!any.isPresent()) {
                    toRemove.add(user);
                }
            }
            channel.removeAll(toRemove);
        }
        client.updateUsers();
    }

    @Override
    public void suspect(Address address) {

        System.out.println("suspect " + address);
    }

    @Override
    public void block() {
        System.out.println("block");
    }

    @Override
    public void unblock() {
        System.out.println("unblock");
    }

    @Override
    public void receive(Message message) {
        try {
            ChatOperationProtos.ChatAction chatAction = ChatOperationProtos.ChatAction.parseFrom(message.getRawBuffer());
            int actionNumber = chatAction.getAction().getNumber();
            String nickname = chatAction.getNickname();
            String channel = chatAction.getChannel();
            List<String> users = chanelUsers.get(channel);
            System.out.println(nickname + " recv");
            if (actionNumber == 0) {
                users.add(nickname);
            } else {
                users.remove(nickname);
            }
            chanelUsers.put(channel, users);
            client.updateUsers();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getState(OutputStream outputStream) throws Exception {
        System.out.println("sending state");
        ChatOperationProtos.ChatState.Builder builder = ChatOperationProtos.ChatState.newBuilder();
        for (Map.Entry<String, List<String>> channelEntry : chanelUsers.entrySet()) {
            for (String user : channelEntry.getValue()) {
                System.out.println(user + " " + channelEntry.getKey());
                ChatOperationProtos.ChatAction action = ChatOperationProtos.ChatAction
                        .newBuilder()
                        .setAction(ChatOperationProtos.ChatAction.ActionType.JOIN)
                        .setNickname(user)
                        .setChannel(channelEntry.getKey())
                        .build();
                builder.addState(action);
            }
        }
        byte[] bytes = builder.build().toByteArray();
        outputStream.write(bytes);
    }

    @Override
    public void setState(InputStream inputStream) throws Exception {

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        int n;
        while (-1 != (n = inputStream.read(buffer))) {
            output.write(buffer, 0, n);
        }
        byte[] bytes = output.toByteArray();

        ChatOperationProtos.ChatState chatState = ChatOperationProtos.ChatState.parseFrom(bytes);
        for (ChatOperationProtos.ChatAction chatAction : chatState.getStateList()) {
            List<String> users = chanelUsers.get(chatAction.getChannel());
            users.add(chatAction.getNickname());
            chanelUsers.put(chatAction.getChannel(), users);
        }
    }

    public Map<String, List<String>> getChanelUsers() {
        return chanelUsers;
    }
}
