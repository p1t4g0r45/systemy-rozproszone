#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFSIZE 16

int main(int argc, char ** argv){
	struct sockaddr_in serv_addr, cli_addr;
	int fd, cli_fd, filed;
	unsigned int cli_len;

	if(argc < 3) {
		printf("usage: %s [port] [file]\n", argv[0]);
		return 1;
	}
	
	if( (filed = open(argv[2], O_RDONLY)) < 0) {
		perror("error");
		return 1;
	}
	
	if( (fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)	{
		printf("error creating socket\n");
		perror("error");
		return 1;
	}
	
	bzero( (char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family		= AF_INET;
	serv_addr.sin_addr.s_addr	= htonl(INADDR_ANY);
	serv_addr.sin_port		= htons(atoi(argv[1]));

	if( bind(fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		printf("error binding socket\n");
		perror("error");
		close(fd);
		return 1;
	}

	listen(fd, 5);

	printf("started\n");

	if( (cli_fd = accept(fd, (struct sockaddr *) &cli_addr, &cli_len)) < 0)	{
		printf("error accepting client\n");
		perror("error");
		close(fd);
		return 1;
	}


	printf("sending filename\t\t");
	char buffer[BUFFSIZE];
	char c = 201;
	size_t len = strlen(argv[2]);
	snprintf(buffer, len+2, "%s%c", argv[2], c);

	if( send(cli_fd, &buffer, len+1, 0) < 0) {
		printf("error sending filename\n");
		perror("error");
		close(filed);
		close(fd);
		return EXIT_FAILURE;
	}

	printf("sent\n");
	printf("sending file\t\t");
	while(1) {
		char buffer[BUFFSIZE];
		ssize_t bytes_read = 0;

		if((bytes_read = read(filed, &buffer, sizeof(buffer))) < 0)	{
			printf("error reading file\n");
			perror("error");
			close(fd);
			return 1;
		}

		if(bytes_read == 0)	{
			close(filed);
			close(fd);
			close(cli_fd);
			printf("sent\n");
			return 0;
		}

		if( send(cli_fd, &buffer, bytes_read, 0) < 0) {
			printf("error sending chunk\n");
			perror("error");
			close(filed);
			close(fd);
			return 1;
		}
	}
	
	return 0;
}
