package Baranski_Jakub_5;

import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.protocols.*;
import org.jgroups.protocols.pbcast.*;
import org.jgroups.stack.Protocol;
import org.jgroups.stack.ProtocolStack;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos.ChatAction;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by baran on 13.05.16.
 */
public class Client {

    private final ChatManagementReceiver receiver;
    JChannel chatManagement;
    private String nickname;
    private Map<String, JChannel> channels;
    private final MainPanel panel;

    public static void main(String[] args) {
        Client client = new Client();
    }

    public Client() {
        this.nickname = UUID.randomUUID().toString();
        try {
            nickname = new BufferedReader(new InputStreamReader(System.in)).readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        receiver = new ChatManagementReceiver(this);
        channels = new HashMap<>();
        try {
            connectToChatManagement(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JFrame frame = new JFrame("Chat");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panel = new MainPanel(this);
        frame.setContentPane(panel);
        frame.setSize(500, 500);
        frame.pack();
        frame.setVisible(true);
    }

    private void connectToChatManagement(boolean createProtocolStack) throws Exception {
        if (!createProtocolStack) {
            chatManagement = new JChannel();
            chatManagement.setName(nickname);
            chatManagement.connect("ChatManagement321123");
            chatManagement.setReceiver(receiver);
            chatManagement.getState(null, 1000);
        } else {
            chatManagement = new JChannel(false);
            ProtocolStack stack = prepareStack(null);
            chatManagement.setProtocolStack(stack);
            stack.init();
            chatManagement.setName(nickname);
            chatManagement.connect("ChatManagement321123");
            chatManagement.setReceiver(receiver);
            chatManagement.getState(null, 1000);
        }
    }

    public boolean isInChannel(String channel) {
        return receiver.getChanelUsers().get(channel).contains(nickname);
    }

    public ChatManagementReceiver getReceiver() {
        return receiver;
    }

    private ProtocolStack prepareStack(String nr) throws Exception {
        ProtocolStack stack = new ProtocolStack();
        Protocol udp = new UDP();
        if (nr != null) {
            udp = new UDP().setValue("mcast_group_addr", InetAddress.getByName("230.0.0." + nr));
        }
        stack.addProtocol(udp)
                .addProtocol(new PING())
                .addProtocol(new MERGE2())
                .addProtocol(new FD_SOCK())
                .addProtocol(new FD_ALL().setValue("timeout", 12000).setValue("interval", 3000))
                .addProtocol(new VERIFY_SUSPECT())
                .addProtocol(new BARRIER())
                .addProtocol(new NAKACK())
                .addProtocol(new UNICAST2())
                .addProtocol(new STABLE())
                .addProtocol(new GMS())
                .addProtocol(new UFC())
                .addProtocol(new MFC())
                .addProtocol(new FRAG2())
                .addProtocol(new STATE_TRANSFER())
                .addProtocol(new FLUSH());
        return stack;
    }

    public void joinChannel(String nr) throws Exception {
        JChannel channel = new JChannel(false);
        ProtocolStack stack = prepareStack(nr);
        channel.setProtocolStack(stack);
        stack.init();
        ChatAction action = ChatAction.newBuilder().setAction(ChatAction.ActionType.JOIN).setChannel(nr).setNickname(this.nickname).build();
        chatManagement.send(new Message(null, null, action.toByteArray()));
        channel.setReceiver(new MessageReceiver(nr, receiver, panel));
        channel.setName(nickname);
        channel.connect(nr);
        this.channels.put(nr, channel);
    }

    public void send(String channel, String text) throws Exception {
        ChatOperationProtos.ChatMessage message = ChatOperationProtos.ChatMessage.newBuilder().setMessage(text).build();
        channels.get(channel).send(new Message(null, null, message.toByteArray()));
    }

    public void updateUsers() {
        panel.updateCurrentUsers();
    }

    public void leaveChannel(String channel) throws Exception {
        channels.get(channel).close();
        channels.remove(channel);
        ChatAction action = ChatAction.newBuilder().setAction(ChatAction.ActionType.LEAVE).setChannel(channel).setNickname(this.nickname).build();
        chatManagement.send(new Message(null, null, action.toByteArray()));
    }
}
