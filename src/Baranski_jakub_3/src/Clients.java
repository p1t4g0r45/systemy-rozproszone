package Baranski_jakub_3.src;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.IOException;
import java.util.*;

/**
 * Created by baran on 09.04.16.
 */
public class Clients   {

    private static final String JNDI_CONTEXT_FACTORY_CLASS_NAME = "org.exolab.jms.jndi.InitialContextFactory";
    private static final String DEFAULT_JMS_PROVIDER_URL = "tcp://localhost:3035/";
    private static final String DEFAULT_TOPIC_NAME = "topic";

    // Application JNDI context
    private Context jndiContext;

    // JMS Administrative objects
    private TopicConnectionFactory topicConnectionfactory;
    private Map<Solvers.TaskType, Topic> topics = new HashMap<>();

    // JMS Client objects
    private List<QueueReceiver> receivers = new ArrayList<>();

    private TopicConnection topicConnection;
    private TopicSession topicSession;
    private Map<Solvers.TaskType, TopicSubscriber> subscribers = new HashMap<>();

    // Business Logic
    private String clientName;

    public Clients() throws NamingException, JMSException {
        this("Client " + new Random().nextInt());
    }

    public Clients(String clientName) throws NamingException, JMSException {
        this(clientName, DEFAULT_JMS_PROVIDER_URL, DEFAULT_TOPIC_NAME);
    }

    public Clients(String clientName, String providerUrl, String topicName) throws NamingException, JMSException {
        this.clientName = clientName;
        initializeJndiContext(providerUrl);
        initializeAdministrativeObjects(topicName);
        initializeJmsClientObjects();
    }

    private void initializeJndiContext(String providerUrl) throws NamingException {
        // JNDI Context
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_CONTEXT_FACTORY_CLASS_NAME);
        props.put(Context.PROVIDER_URL, providerUrl);
        jndiContext = new InitialContext(props);
        System.out.println("JNDI context initialized!");
    }

    private void initializeAdministrativeObjects(String topicName) throws NamingException {
        // ConnectionFactory
        topicConnectionfactory = (TopicConnectionFactory) jndiContext.lookup("ConnectionFactory");
        // Destination

        topics.put(Solvers.TaskType.ADD, (Topic) jndiContext.lookup(topicName + 1));
        topics.put(Solvers.TaskType.SUBTRACT, (Topic) jndiContext.lookup(topicName + 2));
        topics.put(Solvers.TaskType.MULTIPLY, (Topic) jndiContext.lookup(topicName + 3));
        topics.put(Solvers.TaskType.DIVIDE, (Topic) jndiContext.lookup(topicName + 4));
        System.out.println("JMS administrative objects (ConnectionFactory, Destinations) initialized!");
    }

    private void initializeJmsClientObjects() throws JMSException {
        topicConnection = topicConnectionfactory.createTopicConnection();
        topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
        for (Map.Entry<Solvers.TaskType, Topic> taskTypeTopicEntry : topics.entrySet()) {
            subscribers.put(taskTypeTopicEntry.getKey(), topicSession.createSubscriber(taskTypeTopicEntry.getValue()));
        }
        System.out.println("JMS client objects (Session, MessageConsumer) initialized!");
    }

    public void start() throws JMSException, IOException {
        topicConnection.start();
        System.out.println("Connection started - receiving messages possible!");
        for (Map.Entry<Solvers.TaskType, TopicSubscriber> taskTypeTopicSubscriberEntry : subscribers.entrySet()) {
            taskTypeTopicSubscriberEntry.getValue().setMessageListener(new Client(taskTypeTopicSubscriberEntry.getKey()));
        }

        while(true){}
    }

    public void stop() {
        // close the context
        if (jndiContext != null) {
            try {
                jndiContext.close();
            } catch (NamingException exception) {
                exception.printStackTrace();
            }
        }

        // close the connection
    }
}

