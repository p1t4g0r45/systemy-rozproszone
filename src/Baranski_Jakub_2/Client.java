package Baranski_Jakub_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {

    public static GameBoard game;

    private Client() {
    }

    public void play(String nick) {
        try {
            int playerId;
            if (game.playersRegistered() >= 2) {
                System.out.println("Already 2 players playing");
                return;
            }
            /**
             * computer or human
             */
            System.out.print("Do you want to play with a computer(1) or with a human (2): ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String input;
            int i = 0;
            while (i != 1 && i !=2) {
                try {
                    input = br.readLine();
                    i = Integer.parseInt(input);
                    if (i != 1 && i != 2) {
                        throw new NumberFormatException();
                    }
                } catch (NumberFormatException e) {
                    System.out.println("wrong input, try again");
                }
            }
            boolean soloGame = true;
            if (i == 2) {
                soloGame = false;
            }
            /**
             * register
             */
            playerId = nick.hashCode();
            game.register(playerId, nick);

            if (soloGame) {
                new Bot().start();
            }
            /**
             * wait
             */
            int winner = 0;
            System.out.println("Waiting for other player");
            do {
                Thread.sleep(1000);
            } while (game.playersRegistered() != 2);

            /**
             * play
             */
            while (true) {

                //other player
                do {
                    Thread.sleep(1000);
                } while (!game.myTurn(playerId));
                winner = game.checkWinner();
                if (winner != 0 || game.gameEnded()) {
                    break;
                }
                String response = game.getBoard();
                System.out.println("board:\n" + response);

                //players' turn
                String[] coordinates;
                boolean wrongInput = true;
                int x = 0;
                int y = 0;
                do {
                    try {
                        System.out.print("Enter coordinates: ");
                        coordinates = br.readLine().split(",");
                        x = Integer.parseInt(coordinates[0]);
                        y = Integer.parseInt(coordinates[1]);
                        if (!(x >= 0 && x <= 2 && y >= 0 && y <= 2)) {
                            throw new NumberFormatException();
                        }
                        wrongInput = false;
                    } catch (NumberFormatException | IndexOutOfBoundsException e) {
                        System.out.println("wrong input, try again");
                    }
                } while (wrongInput || !game.play(playerId, x, y));
                winner = game.checkWinner();
                if (winner != 0 || game.gameEnded()) {
                    break;
                }
                System.out.println("Waiting for other player");
            }
            /**
             * check result
             */
            if (winner == playerId) {
                System.out.println("You won! " + game.getSecondNick(playerId) + " lost!");
            } else if (winner != 0) {
                System.out.println("You lost! " + game.getSecondNick(playerId) + " won!");
            } else {
                System.out.println("Draw!");
            }
            game.unregister(playerId);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("usage: host port nick");
        }

        String host = args[0]/*(args.length < 1) ? null : args[0]*/;
        try {
            Registry registry = LocateRegistry.getRegistry(host, Integer.parseInt(args[1]));
            game = (GameBoard) registry.lookup("GameBoard");
            Client client = new Client();
            client.play(args[2]);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}