package Baranski_jakub_3.src;

public class GeneratorsMain {
	
	public static void main(String[] args) {
		Generators generators = null;
		try {
			generators = new Generators();
			generators.start();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (generators != null) {
				generators.stop();
			}
		}
	}
}
