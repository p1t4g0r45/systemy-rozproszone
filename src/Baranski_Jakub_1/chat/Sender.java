package Baranski_Jakub_1.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Date;

/**
 * Created by baran on 11.03.16.
 */
public class Sender extends Thread {

    private DatagramSocket socket;
    private InetAddress group;
    private String nick;

    public Sender(DatagramSocket socket, InetAddress group, String nick) {
        this.socket = socket;
        this.group = group;
        this.nick = nick;
    }

    @Override
    public void run() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String input;

            while ((input = br.readLine()) != null) {
                String encode = new Message(nick, input, new Date().toString()).encode();
                socket.send(new DatagramPacket(encode.getBytes(), encode.getBytes().length, group, Main.PORT));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

