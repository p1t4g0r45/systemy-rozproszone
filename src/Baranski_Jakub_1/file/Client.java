package Baranski_Jakub_1.file;

import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;


public class Client {

    public static void main(String args[]) {
        Socket socket = null;
        DataInputStream in;
        FileOutputStream fos = null;

        if (args.length < 2) {
            System.out.println("usage: [host] [port]");
            System.exit(1);
        }

        try {
            socket = new Socket(args[0], Integer.parseInt(args[1]));
            in = new DataInputStream(socket.getInputStream());


            byte[] bytes = new byte[16];
            int bytesRead = in.read(bytes);
            if (bytesRead == -1) {
                System.exit(1);
            }

            String name = "";
            int i;
            for (i = 0; i < bytesRead; i++) {
                if(bytes[i] == -55){
                    i++;
                    break;
                }
                name = name + ((char) bytes[i]);
            }

            fos = new FileOutputStream(name);
            System.out.println("Created file " + name);

            for (; i < bytesRead; i++) {
                fos.write(bytes[i]);
            }

            while (true) {
                bytesRead = in.read(bytes);
                if (bytesRead == -1) {
                    System.out.println("Writing completed");
                    System.exit(0);
                }

                for (i = 0; i < bytesRead; i++)
                    fos.write(bytes[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (socket != null)
                try {
                    socket.close();
                    if (fos != null)
                        fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

    }
}
