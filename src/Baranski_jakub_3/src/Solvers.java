package Baranski_jakub_3.src;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import javax.jms.*;
import javax.jms.Queue;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.exolab.jms.message.TextMessageImpl;

public class Solvers {

    public enum TaskType {
        ADD("+"), SUBTRACT("-"), MULTIPLY("*"), DIVIDE("/");

        private String s;

        TaskType(String s) {
            this.s = s;
        }

        public String getS() {
            return s;
        }
    }

    private static final String JNDI_CONTEXT_FACTORY_CLASS_NAME = "org.exolab.jms.jndi.InitialContextFactory";
    private static final String DEFAULT_JMS_PROVIDER_URL = "tcp://localhost:3035/";
    private static final String DEFAULT_OUTGOING_MESSAGES_QUEUE_NAME = "queue";
    private static final String DEFAULT_TOPIC_NAME = "topic";

    // Application JNDI context
    private Context jndiContext;

    // JMS Administrative objects
    private QueueConnectionFactory queueConnectionFactory;
    private List<Queue> incommingMessagesQueues = new ArrayList<>();
    private TopicConnectionFactory topicConnectionfactory;
    private Map<TaskType, Topic> topics = new HashMap<>();

    // JMS Client objects
    private QueueConnection connection;
    private QueueSession session;
    //	private QueueSender sender;
    private List<QueueReceiver> receivers = new ArrayList<>();

    private TopicConnection topicConnection;
    private TopicSession topicSession;
    private Map<TaskType, TopicPublisher> publishers = new HashMap<>();

    // Business Logic
    private String clientName;

    public Solvers() throws NamingException, JMSException {
        this("Client " + new Random().nextInt());
    }

    public Solvers(String clientName) throws NamingException, JMSException {
        this(clientName, DEFAULT_JMS_PROVIDER_URL, DEFAULT_OUTGOING_MESSAGES_QUEUE_NAME, DEFAULT_TOPIC_NAME);
    }

    public Solvers(String clientName, String providerUrl, String outgoingMessagesQueueName, String topicName) throws NamingException, JMSException {
        this.clientName = clientName;
        initializeJndiContext(providerUrl);
        initializeAdministrativeObjects(outgoingMessagesQueueName, topicName);
        initializeJmsClientObjects();
    }

    private void initializeJndiContext(String providerUrl) throws NamingException {
        // JNDI Context
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_CONTEXT_FACTORY_CLASS_NAME);
        props.put(Context.PROVIDER_URL, providerUrl);
        jndiContext = new InitialContext(props);
        System.out.println("JNDI context initialized!");
    }

    private void initializeAdministrativeObjects(String outgoingMessagesQueueName, String topicName) throws NamingException {
        // ConnectionFactory
        queueConnectionFactory = (QueueConnectionFactory) jndiContext.lookup("ConnectionFactory");
        topicConnectionfactory = (TopicConnectionFactory) jndiContext.lookup("ConnectionFactory");
        // Destination
        for (int i = 0; i < Config.m; i++) {
            incommingMessagesQueues.add((Queue) jndiContext.lookup(outgoingMessagesQueueName + i));
        }
        topics.put(TaskType.ADD, (Topic) jndiContext.lookup(topicName + 1));
        topics.put(TaskType.SUBTRACT, (Topic) jndiContext.lookup(topicName + 2));
        topics.put(TaskType.MULTIPLY, (Topic) jndiContext.lookup(topicName + 3));
        topics.put(TaskType.DIVIDE, (Topic) jndiContext.lookup(topicName + 4));
        System.out.println("JMS administrative objects (ConnectionFactory, Destinations) initialized!");
    }

    private void initializeJmsClientObjects() throws JMSException {
        connection = queueConnectionFactory.createQueueConnection();
        topicConnection = topicConnectionfactory.createTopicConnection();
        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
        for (Queue incommingMessagesQueue : incommingMessagesQueues) {
            receivers.add(session.createReceiver(incommingMessagesQueue));
        }
        for (Map.Entry<TaskType, Topic> taskTypeTopicEntry : topics.entrySet()) {
            publishers.put(taskTypeTopicEntry.getKey(), topicSession.createPublisher(taskTypeTopicEntry.getValue()));
        }
        System.out.println("JMS client objects (Session, MessageConsumer) initialized!");
    }

    public void start() throws JMSException, IOException {
        topicConnection.start();
        connection.start();
        System.out.println("Connection started - receiving messages possible!");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        for (int i = 0; i < Config.n; i++) {
            new Solver(i, receivers, publishers).start();
        }

        while (true) {
        }
    }

    public void stop() {
        // close the context
        if (jndiContext != null) {
            try {
                jndiContext.close();
            } catch (NamingException exception) {
                exception.printStackTrace();
            }
        }

        // close the connection
        if (connection != null) {
            try {
                connection.close();
            } catch (JMSException exception) {
                exception.printStackTrace();
            }
        }
    }

}
