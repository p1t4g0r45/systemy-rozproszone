#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFSIZE 16
#define SCALE 10000  
#define ARRINIT 2000 

int pi_digits(int digits, int n);

int main(int argc, char ** argv){
	struct sockaddr_in serv_addr, cli_addr;
	int fd, cli_fd;
	unsigned int cli_len;

	if(argc < 2) {
		printf("usage: %s [port]\n", argv[0]);
		return 1;
	}
	
	if( (fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)	{
		printf("error creating socket\n");
		perror("error");
		return 1;
	}
	
	bzero( (char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family		= AF_INET;
	serv_addr.sin_addr.s_addr	= htonl(INADDR_ANY);
	serv_addr.sin_port		= htons(atoi(argv[1]));

	if( bind(fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		printf("error binding socket\n");
		perror("error");
		close(fd);
		return 1;
	}

	listen(fd, 5);

	printf("started\n");

	if( (cli_fd = accept(fd, (struct sockaddr *) &cli_addr, &cli_len)) < 0)	{
		printf("error accepting client\n");
		perror("error");
		close(fd);
		return 1;
	}


	while(1){
		char buffer[BUFFSIZE];
		ssize_t n;
		memset(buffer, 0, BUFFSIZE);
			
		if( (n = recv(cli_fd, &buffer, (size_t) cli_len, 0) ) < 0 ) {
			perror("error receiving");
			close(cli_fd);
			close(fd);
			return 1;
		} else if(n == 0){
			printf("connection closed\n");
			close(cli_fd);
			close(fd);
			return 0;
		}


	
		char tmp = (char)buffer[0];
		int t = 0;
		printf("received: %d\n", buffer[0]);
		tmp = pi_digits(1000, buffer[0]);

		printf("Sending %d\t\t\t", tmp);

		if( (t = send(cli_fd, &tmp, sizeof(tmp), 0)) < 0) {
			printf("error sending\n");
			perror("error");
			close(fd);
			return EXIT_FAILURE;
		}

		printf("sent %d bytes\n", t);
				
	}

	
	return 0;
}

int pi_digits(int digits, int n) {  
	n = n+1; //leading 3,
    int carry = 0;  
    int arr[digits + 1];  
    for (int i = 0; i <= digits; ++i)  
        arr[i] = ARRINIT;  
    for (int i = digits; i > 0; i-= 14) {  
        int sum = 0;  
        for (int j = i; j > 0; --j) {  
            sum = sum * j + SCALE * arr[j];  
            arr[j] = sum % (j * 2 - 1);  
            sum /= j * 2 - 1;  
        }
		int res= carry + sum / SCALE;
		if(n==1){
			return res / 1000;
		} else if(n==2) {
			return (res / 100) % 10;
		} else if (n==3){
			return (res / 10) % 10;
		} else if (n==4){
			return res %10;
		}
		n = n-4;
        printf("%d\n", res);  
        carry = sum % SCALE;  
    }  
	return -1;
}  
  

