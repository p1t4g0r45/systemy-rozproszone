package Baranski_Jakub_5;

import com.google.protobuf.InvalidProtocolBufferException;
import org.jgroups.*;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by baran on 14.05.16.
 */
public class MessageReceiver implements Receiver {

    private String nr;
    private ChatManagementReceiver chatManagement;
    private MainPanel panel;

    public MessageReceiver(String nr, ChatManagementReceiver chatManagement, MainPanel panel) {
        this.nr = nr;
        this.chatManagement = chatManagement;
        this.panel = panel;
    }

    @Override
    public void viewAccepted(View view) {

    }

    @Override
    public void suspect(Address address) {

    }

    @Override
    public void block() {

    }

    @Override
    public void unblock() {

    }

    @Override
    public void receive(Message message) {
        try {
            ChatOperationProtos.ChatMessage chatMessage = ChatOperationProtos.ChatMessage.parseFrom(message.getBuffer());
            panel.newMessage(nr, message.getSrc(), chatMessage);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getState(OutputStream outputStream) throws Exception {

    }

    @Override
    public void setState(InputStream inputStream) throws Exception {

    }
}
