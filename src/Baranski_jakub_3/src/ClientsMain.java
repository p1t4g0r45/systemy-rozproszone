package Baranski_jakub_3.src;

/**
 * Created by baran on 09.04.16.
 */
public class ClientsMain {
    public static void main(String[] args) {
        Clients clients = null;
        try {
            clients = new Clients();
            clients.start();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (clients != null) {
                clients.stop();
            }
        }

    }

}
