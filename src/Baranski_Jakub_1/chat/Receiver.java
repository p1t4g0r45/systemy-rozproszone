package Baranski_Jakub_1.chat;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Created by baran on 11.03.16.
 */
public class Receiver extends Thread {

    private DatagramSocket socket;
    private String nick;

    public Receiver(DatagramSocket socket, String nick) {
        this.socket = socket;
        this.nick = nick;
    }

    @Override
    public void run() {
        try {
            while (true) {
                DatagramPacket packet;
                byte[] buf = new byte[256];
                packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);

                String received = new String(packet.getData());
                Message message = Message.decode(received);
                if(message.getCheckSum() != Message.calcuateSum(message)){
                    System.out.println("Received broken message");
                    continue;
                }
                if(!message.getNick().equals(nick)){
                    System.out.println(message.getNick() + " (" + message.getDate() + ") :");
                    System.out.println(message.getContent());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
