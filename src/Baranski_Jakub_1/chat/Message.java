package Baranski_Jakub_1.chat;

import java.util.Arrays;
import java.util.List;

/**
 * Created by baran on 11.03.16.
 */
public class Message {

    private static final char DELIMITER = 201;

    private String nick;
    private String content;
    private String date;
    private int checkSum;

    public Message(String nick, String content, String date) {
        this.date = date;
        this.nick = nick;
        this.content = content;
    }

    public Message(String nick, String content, String date, int sum) {
        this.date = date;
        this.nick = nick;
        this.content = content;
        this.checkSum = sum;
    }

    public static int calcuateSum(Message msg) {
        int sum = msg.getNick().hashCode() % 1000000;
        sum = sum * 31 + msg.getContent().hashCode() % 100000;
        sum = sum * 31 + msg.getDate().hashCode() % 100000;
        return sum;
    }

    public String encode() {
        return nick + DELIMITER + content + DELIMITER + date + DELIMITER + calcuateSum(this) + DELIMITER;
    }

    public static Message decode(String msg) {
        List<String> strings = Arrays.asList(msg.split(String.valueOf(DELIMITER)));
        return new Message(strings.get(0), strings.get(1), strings.get(2), Integer.parseInt(strings.get(3)));
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(int checkSum) {
        this.checkSum = checkSum;
    }
}
