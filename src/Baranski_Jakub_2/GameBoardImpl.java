package Baranski_Jakub_2;

import java.rmi.RemoteException;

/**
 * Created by baran on 18.03.16.
 */
public class GameBoardImpl implements GameBoard {

    private int board[][];
    private boolean firstPlayer = true; // true - first, false - second
    private int firstPlayerId;
    private int secondPlayerId;
    private String firstPlayerNick;
    private String secondPlayerNick;

    public GameBoardImpl() {
        board = new int[3][3];
    }

    public boolean gameEnded() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean myTurn(int playerId) {
        return (firstPlayerId == playerId && firstPlayer) || (secondPlayerId == playerId && !firstPlayer);
    }

    public int playersRegistered() {
        return firstPlayerId == 0 ? 0 : secondPlayerId == 0 ? 1 : 2;
    }

    public boolean register(int playerId, String nick) {
        if (firstPlayerId == 0) {
            firstPlayerId = playerId;
            firstPlayerNick = nick;
        } else if (secondPlayerId == 0 && playerId != firstPlayerId) {
            secondPlayerId = playerId;
            secondPlayerNick = nick;
        } else {
            return false;
        }
        return true;
    }

    public void unregister(int playerId) {
        if (playerId == secondPlayerId) {
            secondPlayerId = 0;
        } else if (playerId == firstPlayerId) {
            firstPlayerId = 0;
        }
        if(firstPlayerId == 0 && secondPlayerId == 0){
            firstPlayer = true;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    board[i][j] = 0;
                }
            }
        }
    }

    public boolean play(int player, int i, int j) {
        if (firstPlayer && player != firstPlayerId) {
            return false;
        }
        if (board[i][j] != 0) {
            return false;
        }
        board[i][j] = player;
        firstPlayer = !firstPlayer;
        return true;
    }

    public int checkWinner() {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] != 0) {
                return board[i][0];
            }
            if (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] != 0) {
                return board[0][i];
            }
        }
        if (board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] != 0) {
            return board[0][0];
        }
        if (board[2][0] == board[1][1] && board[2][0] == board[0][2] && board[2][0] != 0) {
            return board[2][0];
        }
        return 0;
    }

    public String getBoard() throws RemoteException {
        String s = "";
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                char c = board[i][j] == 0 ? '-' : board[i][j] == firstPlayerId ? 'X' : 'O';
                s = s + c + " ";
            }
            s = s + "\n";
        }
        return s;
    }

    public String getSecondNick(int playerId) throws RemoteException {
        if(playerId == firstPlayerId){
            return secondPlayerNick;
        } else if (playerId == secondPlayerId){
            return firstPlayerNick;
        }
        return "";
    }
}
