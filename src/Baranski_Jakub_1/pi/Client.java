package Baranski_Jakub_1.pi;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;


public class Client {

    public static void main(String args[]) {
        Socket socket = null;
        DataOutputStream out;
        DataInputStream in;

        if (args.length < 2) {
            System.out.println("usage: [host] [port]");
            System.exit(1);
        }

        try {
            socket = new Socket(args[0], Integer.parseInt(args[1]));
            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());

            byte[] b = new byte[1];
            b[0]=5;
            System.out.println("sending: " + b[0]);
            out.write(b);
            int res = in.read();
            System.out.println("received: " + res);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}