package Baranski_jakub_3.src;

public class SolversMain {
	
	public static void main(String[] args) {
		Solvers solvers = null;
		try {
			solvers = new Solvers();
			solvers.start();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (solvers != null) {
				solvers.stop();
			}
		}
		
	}
	
}
