package Baranski_Jakub_6;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by baran on 26.05.16.
 */
public class Main implements Watcher, Runnable {

    public static boolean printOnChange = true;
    private static String connectionString = "127.0.0.1:2181";
    public static final String path = "/znode_testowy";
    private ZooKeeper zooKeeper;
    private DescendantWatcher testWatcher;
    private static String app;
    private Process exec;

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("no callback given");
            return;
        }
        app = args[0];
        new Main();
    }

    public Main() {
        try {
            zooKeeper = new ZooKeeper(connectionString, 5000, this);
            zooKeeper.getChildren("/", this);
            new Thread(this).start();
        } catch (IOException | InterruptedException | KeeperException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void process(WatchedEvent event) {
        try {
            Stat exists = zooKeeper.exists(path, false);
            if (exists == null) {
                if (testWatcher != null) {
                    testWatcher = null;
                    if (exec != null) {
                        exec.destroy();
                    }
                }
            } else {
                if (testWatcher == null) {
                    try {
                        exec = Runtime.getRuntime().exec(app);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    testWatcher = new DescendantWatcher(zooKeeper, path);
                    zooKeeper.getChildren(path, testWatcher);
                }
            }/*
            if(printOnChange){
                printTree(zooKeeper, "/");
            }*/
            zooKeeper.getChildren("/", this);
        } catch (KeeperException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String line = "";
        try {
            while (!(line = bufferedReader.readLine()).equals("quit")) {
                if (line.equals("print")) {
                    printTree(zooKeeper, path);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void printTree(ZooKeeper zooKeeper, String s) {
        System.out.println("-----------------");
        int count = printChildren(zooKeeper, s, 0);
        System.out.println("Sum: " + (count == 0 ? 0 : count - 1));
    }

    private static int printChildren(ZooKeeper zooKeeper, String s, int level) {
        try {
            int count = 0;
            List<String> children = zooKeeper.getChildren(s, false);
            for (String child : children) {
                for (int i = 0; i < level; i++) {
                    System.out.print("--");
                }
                System.out.println(child);
                if (s.equals("/")) {
                    s = "";
                }
                count += printChildren(zooKeeper, s + "/" + child, level + 1);
            }
            return count + 1;
        } catch (KeeperException | InterruptedException e) {
            return 0;
        }
    }
}
