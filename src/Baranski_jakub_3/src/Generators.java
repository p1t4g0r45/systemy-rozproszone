package Baranski_jakub_3.src;

import org.exolab.jms.message.TextMessageImpl;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

public class Generators {

    private static final String JNDI_CONTEXT_FACTORY_CLASS_NAME = "org.exolab.jms.jndi.InitialContextFactory";
    private static final String DEFAULT_JMS_PROVIDER_URL = "tcp://localhost:3035/";
    private static final String DEFAULT_INCOMING_MESSAGES_QUEUE_NAME = "queue";

    // Application JNDI context
    private Context jndiContext;

    // JMS Administrative objects references
    private QueueConnectionFactory queueConnectionFactory;
    private List<Queue> outgoingMessagesQueues = new ArrayList<>();
    //private Topic topic;

    // JMS Client objects
    private QueueConnection connection;
    private QueueSession session;

    private List<QueueSender> senders = new ArrayList<>();


    /**************
     * Initialization BEGIN
     ******************************/
    public Generators() throws NamingException, JMSException {
        this(DEFAULT_JMS_PROVIDER_URL, DEFAULT_INCOMING_MESSAGES_QUEUE_NAME);
    }

    public Generators(String providerUrl, String incomingMessagesQueueName) throws NamingException, JMSException {
        initializeJndiContext(providerUrl);
        initializeAdministrativeObjects(incomingMessagesQueueName);
        initializeJmsClientObjects();
    }

    private void initializeJndiContext(String providerUrl) throws NamingException {
        // JNDI Context
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_CONTEXT_FACTORY_CLASS_NAME);
        props.put(Context.PROVIDER_URL, providerUrl);
        jndiContext = new InitialContext(props);
        System.out.println("JNDI context initialized!");
    }

    private void initializeAdministrativeObjects(String incomingMessagesQueueName) throws NamingException {
        // ConnectionFactory
        queueConnectionFactory = (QueueConnectionFactory) jndiContext.lookup("ConnectionFactory");
        // Destination
        for (int i = 0; i < Config.m; i++) {
            outgoingMessagesQueues.add((Queue) jndiContext.lookup(incomingMessagesQueueName + i));
        }
        System.out.println("JMS administrative objects (ConnectionFactory, Destinations) initialized!");
    }

    private void initializeJmsClientObjects() throws JMSException {
        connection = queueConnectionFactory.createQueueConnection();
        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE); // false - non-transactional, AUTO_ACKNOWLEDGE - messages acknowledged after receive() method returns
        for (Queue incomingMessagesQueue : outgoingMessagesQueues) {
            senders.add(session.createSender(incomingMessagesQueue));
        }
        System.out.println("JMS client objects (Session, MessageConsumer) initialized!");
    }
    /************** Initialization END ******************************/


    /**************
     * Business logic BEGIN
     ****************************/
    public void start() throws JMSException, IOException, InterruptedException {
        connection.start();
        System.out.println("Connection started - enter input:");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;
        /*while ((line = br.readLine()) != null) {
            String[] split = line.split(" ");
            int generator = Integer.parseInt(split[0]);
            TextMessage textMessage = new TextMessageImpl();
            String text = split[1];
            textMessage.setText(text);
            Solvers.TaskType taskType;
            if (text.contains("+")) {
                taskType = Solvers.TaskType.ADD;
            } else if (text.contains("-")) {
                taskType = Solvers.TaskType.SUBTRACT;
            } else if (text.contains("*")) {
                taskType = Solvers.TaskType.MULTIPLY;
            } else {
                taskType = Solvers.TaskType.DIVIDE;
            }
            textMessage.setStringProperty("type", taskType.getS());
            senders.get(generator).send(textMessage);
        }*/
        Random rand = new Random();
        String[] op = new String[] {"+", "-", "*", "/"};
        while(true){
            int generatorId = rand.nextInt(Config.m);
            int a = rand.nextInt(5);
            int b = rand.nextInt(5);
            int c = rand.nextInt(4);
            TextMessageImpl textMessage = new TextMessageImpl();
            textMessage.setText( a + op[c] + b);
            Solvers.TaskType taskType;
            if (c == 0) {
                taskType = Solvers.TaskType.ADD;
            } else if (c == 1) {
                taskType = Solvers.TaskType.SUBTRACT;
            } else if (c ==2) {
                taskType = Solvers.TaskType.MULTIPLY;
            } else {
                taskType = Solvers.TaskType.DIVIDE;
            }
            textMessage.setStringProperty("type", taskType.getS());
            senders.get(generatorId).send(textMessage);
        }
    }

    public void stop() {
        // close the connection
        if (connection != null) {
            try {
                connection.close();
            } catch (JMSException exception) {
                exception.printStackTrace();
            }
        }

        // close the context
        if (jndiContext != null) {
            try {
                jndiContext.close();
            } catch (NamingException exception) {
                exception.printStackTrace();
            }
        }
    }
    /************** Business logic END ****************************/

}
