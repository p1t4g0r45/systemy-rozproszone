package Baranski_Jakub_5;

import org.jgroups.Address;
import pl.edu.agh.dsrg.sr.chat.protos.ChatOperationProtos;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by baran on 14.05.16.
 */
public class MainPanel extends JPanel implements ItemListener, ActionListener {

    private final JTextPane users;
    private final JTextPane text;
    private final JTextField inputField;
    private Client client;
    private final JButton join;
    private final JComboBox<String> chanelList;
    private final Map<String, String> messages;

    public MainPanel(Client client) {
        super();
        this.client = client;
        String[] strings = new String[200];
        for (int i = 1; i <= 200; i++) {
            strings[i - 1] = String.valueOf(i);
        }
        setPreferredSize(new Dimension(500, 500));
        chanelList = new JComboBox<>(strings);
        chanelList.addItemListener(this);
        users = new JTextPane();
        users.setAlignmentY(0);
        users.setPreferredSize(new Dimension(100, 500));
        JScrollPane userScroller = new JScrollPane(users, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        join = new JButton("Join");
        join.addActionListener(this);
        text = new JTextPane();
        text.setPreferredSize(new Dimension(300, 500));
        JScrollPane textScroller = new JScrollPane(text, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        inputField = new JTextField(50);
        JButton send = new JButton("Send");
        send.addActionListener(this);
        add(chanelList);
        add(userScroller);
        add(join);
        add(new JLabel("---"));
        add(textScroller);
        add(inputField);
        add(send);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        messages = new HashMap<>();

        updateUsers("1");
        updateText("1");
    }

    @Override
    public void itemStateChanged(ItemEvent itemEvent) {
        if (itemEvent.getStateChange() == ItemEvent.DESELECTED) {
            return;
        }
        String channel = String.valueOf(itemEvent.getItem());
        updateUsers(channel);
        if (client.isInChannel(channel)) {
            updateText(channel);
            join.setText("Leave");
        } else {
            join.setText("Join");
            text.setText("");
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if ("Join".equals(actionEvent.getActionCommand())) {
                try {
                    String channel = String.valueOf(chanelList.getSelectedItem());
                    client.joinChannel(channel);
                    join.setText("Leave");
                    messages.put(channel, "");
                } catch (Exception e) {
                    e.printStackTrace();
                    users.setText("error");
                }

        } else if ("Leave".equals(actionEvent.getActionCommand())){
            try {
                String channel = String.valueOf(chanelList.getSelectedItem());
                client.leaveChannel(channel);
                join.setText("Join");
            } catch (Exception e) {
                e.printStackTrace();
                users.setText("error");
            }
        } else if ("Send".equals(actionEvent.getActionCommand())) {
            try {
                client.send(String.valueOf(chanelList.getSelectedItem()), inputField.getText());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updateCurrentUsers() {
        updateUsers(String.valueOf(chanelList.getSelectedItem()));
    }

    private void updateUsers(String channel) {
        users.setText(client.getReceiver().getChanelUsers().get(channel).stream().reduce((a, b) -> a + "\n" + b).orElse(""));
    }

    private void updateText(String channel) {
        text.setText(messages.get(channel));
    }

    public void newMessage(String nr, Address src, ChatOperationProtos.ChatMessage chatMessage) {
        String s = messages.get(nr);
        messages.put(nr, s + "\n" + src + ": " + chatMessage.getMessage());
        if (chanelList.getSelectedItem().equals(nr)) {
            updateText(nr);
        }
    }
}
