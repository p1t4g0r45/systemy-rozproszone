package Baranski_jakub_3.src;

import javax.jms.*;

/**
 * Created by baran on 09.04.16.
 */
public class Client implements MessageListener {

    private Solvers.TaskType key;

    public Client(Solvers.TaskType key) {

        this.key = key;
    }

    @Override
    public void onMessage(Message arg0) {
        TextMessage msg = (TextMessage) arg0;
        try {
            System.out.println("(Topic " + key.getS() + "): " + msg.getText());
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
