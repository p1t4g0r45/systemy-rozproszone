package Baranski_Jakub_6;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by baran on 26.05.16.
 */
public class DescendantWatcher implements Watcher {

    private ZooKeeper zooKeeper;
    private String path;
    private Map<String, Watcher> watchers;

    public DescendantWatcher(ZooKeeper zooKeeper, String path) {
        this.zooKeeper = zooKeeper;
        this.path = path;
        this.watchers = new HashMap<>();
    }

    @Override
    public void process(WatchedEvent event) {
        //System.out.println("Descendant watcher: " + event.toString());
        try {
            if (event.getType() != Event.EventType.NodeDeleted) {
                List<String> children = zooKeeper.getChildren(path, false);
                if (event.getType() == Event.EventType.NodeChildrenChanged) {
                    List<String> toRemove = new ArrayList<>();
                    for (String watcher : watchers.keySet()) {
                        if(!children.contains(watcher)){
                            toRemove.add(watcher);
                        }
                    }
                    for (String s : toRemove) {
                        watchers.remove(s);
                    }
                    for (String child : children) {
                        if(!watchers.containsKey(child)){
                            DescendantWatcher newWatcher = new DescendantWatcher(zooKeeper, path + "/" + child);
                            watchers.put(child, newWatcher);
                            zooKeeper.getChildren(path + "/" + child, newWatcher);
                        }
                    }
                }
                if(Main.printOnChange){
                    Main.printTree(zooKeeper, Main.path);
                }
                zooKeeper.getChildren(path, this);
            }
        } catch (KeeperException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
