package Baranski_Jakub_2;

import java.util.Random;

/**
 * Created by baran on 19.03.16.
 */
public class Bot extends Thread {

    public Bot() {
    }

    public void run() {
        try {
            int playerId;
            if (Client.game.playersRegistered() >= 2) {
                System.out.println("Already 2 players playing");
                return;
            }
            /**
             * register
             */
            String nick = "computer";
            playerId = nick.hashCode();
            Client.game.register(playerId, nick);

            /**
             * wait
             */
            int winner = 0;
            do {
                Thread.sleep(1000);
            } while (Client.game.playersRegistered() != 2);

            /**
             * play
             */
            while (true) {

                //other player
                do {
                    Thread.sleep(1000);
                } while (!Client.game.myTurn(playerId));
                winner = Client.game.checkWinner();
                if (winner != 0 || Client.game.gameEnded()) {
                    break;
                }

                //players' turn
                int x, y;
                Random r = new Random();
                do {
                    x = r.nextInt(3);
                    y = r.nextInt(3);
                } while (!Client.game.play(playerId, x, y));
                winner = Client.game.checkWinner();
                if (winner != 0 || Client.game.gameEnded()) {
                    break;
                }
            }
            /**
             * check result
             */
            Client.game.unregister(playerId);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}