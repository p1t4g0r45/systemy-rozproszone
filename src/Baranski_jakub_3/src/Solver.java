package Baranski_jakub_3.src;

import Baranski_Jakub_1.chat.*;
import jdk.nashorn.internal.scripts.JS;
import org.exolab.jms.message.TextMessageImpl;

import javax.jms.*;
import javax.jms.Message;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by baran on 09.04.16.
 */
public class Solver extends Thread {


    private int solverId;
    private List<QueueReceiver> receivers;
    private Map<Solvers.TaskType, TopicPublisher> publishers;

    public Solver(int i, List<QueueReceiver> receivers, Map<Solvers.TaskType, TopicPublisher> publishers) {
        solverId = i;
        this.receivers = receivers;
        this.publishers = publishers;
    }

    @Override
    public void run() {
        try {
            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("JavaScript");
            while (true) {

                Optional<TextMessage> task = getTask();
                if(task.isPresent()){
                    TextMessageImpl textMessage = new TextMessageImpl();
                    String text = task.get().getText();
                    System.out.println(solverId + " got message");
                    textMessage.setText(task.get().getText() + " solved by #" + solverId + ". Result: " + engine.eval(text));
                    String s = task.get().getStringProperty("type");
                    Solvers.TaskType taskType;
                    if (s.equals("+")) {
                        taskType = Solvers.TaskType.ADD;
                    } else if (s.equals("-")) {
                        taskType = Solvers.TaskType.SUBTRACT;
                    } else if (s.equals("*")) {
                        taskType = Solvers.TaskType.MULTIPLY;
                    } else {
                        taskType = Solvers.TaskType.DIVIDE;
                    }
                    publishers.get(taskType).publish(textMessage);
                }

                //Thread.sleep(3000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Optional<TextMessage> getTask() throws JMSException {
        for (QueueReceiver receiver : receivers) {
            Message message = receiver.receiveNoWait();
            if (message != null) {
                return Optional.of((TextMessage) message);
            }
        }
        return Optional.empty();
    }
}
