package Baranski_Jakub_1.chat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * Created by baran on 11.03.16.
 */
public class Main {

    public static void main(String[] args) {
        new Main();
    }


    public static final String IP = "224.0.113.0";
    public static final int PORT = 4446;

    public Main() {
        try {
            System.out.print("Enter nick: ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String nick = br.readLine();
            MulticastSocket socket = new MulticastSocket(PORT);
            InetAddress group = InetAddress.getByName(IP);
            socket.joinGroup(group);
            new Sender(socket, group, nick).start();
            new Receiver(socket, nick).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
