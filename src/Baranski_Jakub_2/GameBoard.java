package Baranski_Jakub_2;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GameBoard extends Remote {
    void unregister(int playerId) throws RemoteException;

    boolean gameEnded() throws RemoteException;

    int checkWinner() throws RemoteException;

    boolean myTurn(int playerId) throws RemoteException;

    int playersRegistered() throws RemoteException;

    boolean register(int playerId, String nick) throws RemoteException;

    boolean play(int player, int i, int j) throws RemoteException;

    String getBoard() throws RemoteException;

    String getSecondNick(int playerId) throws RemoteException;
}